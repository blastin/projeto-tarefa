#!/bin/bash 

#Remover package anterior
bash mvnw clean

# Empacotar Projeto
bash mvnw package -DskipTests

#Levantar Projeto
docker-compose up --build

#Derrubar Projeto
docker-compose down
