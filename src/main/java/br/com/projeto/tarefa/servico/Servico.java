package br.com.projeto.tarefa.servico;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.projeto.tarefa.dominio.Mensagem;
import br.com.projeto.tarefa.repositorio.Repositorio;

@Service
public class Servico {

	private Logger logger = LoggerFactory.getLogger(Servico.class);

	@Autowired
	private Repositorio repositorio;

	public Mensagem postarMensagem(Mensagem mensagem) {
		logger.info("Serviço: cadastrar mensagem em repositorio");
		return repositorio.save(mensagem);
	}

	public List<Mensagem> obter10UltimasMensagens() {
		logger.info("obtendo as dez ultimas mensagens em order de baixo para cima");
		return repositorio.obter10UltimasMensagens();
	}

}
