package br.com.projeto.tarefa.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "Mensagens")
public class Mensagem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long codigo;

	@NotNull
	@Size(min = 3, max = 80)
	private String valor;

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public long getCodigo() {
		return codigo;
	}

	public int comparar(Mensagem mensagem) {
		return Long.compare(mensagem.codigo, codigo);
	}

	@Override
	public String toString() {
		return "Mensagem [codigo=" + codigo + ", valor='" + valor + "']";
	}

}
