package br.com.projeto.tarefa.repositorio;

import java.util.List;

import org.springframework.data.domain.PageRequest;

import br.com.projeto.tarefa.dominio.Mensagem;

public interface Repositorio extends RepositorioDefault {

	public default List<Mensagem> obter10UltimasMensagens() {
		return obterMensagensOrdenadaDeBaixoParaCima(PageRequest.of(0, 10));
	}

}
