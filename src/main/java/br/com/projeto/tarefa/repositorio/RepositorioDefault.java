package br.com.projeto.tarefa.repositorio;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.projeto.tarefa.dominio.Mensagem;

interface RepositorioDefault extends JpaRepository<Mensagem, Long> {

	@Query("select m from Mensagem m order by m.codigo desc")
	public List<Mensagem> obterMensagensOrdenadaDeBaixoParaCima(Pageable pageable);
}
