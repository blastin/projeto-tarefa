package br.com.projeto.tarefa.excessoes;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExcessaoHandle extends ResponseEntityExceptionHandler {

	private Logger log = LoggerFactory.getLogger(ExcessaoHandle.class);

	@ExceptionHandler
	public void mensagemInvalida(ConstraintViolationException constraintViolationException) {
		log.info("Violacao: {}", constraintViolationException.getConstraintViolations().parallelStream().findFirst()
				.orElseThrow(RuntimeException::new).getMessage());
	}

	@ExceptionHandler
	public void exceptionHandle(RuntimeException e) {
		log.info("Erro de runtime: {}", e.getMessage());
	}
}
