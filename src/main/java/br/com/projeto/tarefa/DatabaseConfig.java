package br.com.projeto.tarefa;

import java.net.URI;
import java.net.URISyntaxException;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseConfig {

	@Bean
	public DataSource dataSource() {

		BasicDataSource basicDataSource;

		try {
			basicDataSource = databaseURL();
		} catch (Exception e) {

			basicDataSource = new BasicDataSource();

			basicDataSource.setUrl(System.getenv("JDBC_DATABASE_URL"));
			basicDataSource.setUsername(System.getenv("JDBC_DATABASE_USERNAME"));
			basicDataSource.setPassword(System.getenv("JDBC_DATABASE_PASSWORD"));

		}

		return basicDataSource;

	}

	private BasicDataSource databaseURL() throws URISyntaxException {

		String url = System.getenv("DATABASE_URL");
		URI dbUri = new URI(url);
		String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();

		BasicDataSource basicDataSource = new BasicDataSource();

		basicDataSource.setUrl(dbUrl);
		basicDataSource.setUsername(dbUri.getUserInfo().split(":")[0]);
		basicDataSource.setPassword(dbUri.getUserInfo().split(":")[1]);

		return basicDataSource;

	}
}