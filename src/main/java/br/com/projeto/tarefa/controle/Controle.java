package br.com.projeto.tarefa.controle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.projeto.tarefa.dominio.Mensagem;
import br.com.projeto.tarefa.servico.Servico;

@Controller
public class Controle {

	private Logger logger = LoggerFactory.getLogger(Controle.class);

	@Autowired
	private Servico servico;

	@GetMapping("/")
	public ModelAndView obter10UltimasMensagens() {
		logger.info("Requisição de mensagens");
		return new ModelAndView("index").addObject("postagens", servico.obter10UltimasMensagens());
	}

	@PostMapping("/api/mensagem/postar")
	public @ResponseBody Mensagem postarMensagem(@RequestParam("mensagem") String mensagemString) {

		Mensagem mensagem = new Mensagem();
		mensagem.setValor(mensagemString);

		logger.info("Postagem de mensagem : {}", mensagem);

		return servico.postarMensagem(mensagem);
	}

}
