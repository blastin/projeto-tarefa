package br.com.projeto.tarefa.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class Interceptor implements HandlerInterceptor {

	private Logger logger = LoggerFactory.getLogger(Interceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		logger.info("Inteceptor Local Addr: {}", request.getLocalAddr());
		logger.info("Inteceptor Remote Addr: {}", request.getRemoteAddr());

		return HandlerInterceptor.super.preHandle(request, response, handler);
	}
}
