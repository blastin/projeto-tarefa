package br.com.projeto.tarefa.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = DatabaseTestConfig.class)
public class TarefaApplicationTests {

	@Test
	public void contextLoads() {
	}

}
