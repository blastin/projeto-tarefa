#Base Imagem
FROM lisboajeff/alpine-openjdk8-jre:latest

#COPIAR JAR PARA CONTAINER
COPY target/*.war tarefa.war

#COMANDO DE INICIALIZAÇÃO
CMD [ "/usr/lib/jvm/java-1.8-openjdk/bin/java", "-jar", "tarefa.war" ]
