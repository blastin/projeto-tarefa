#   [PROJETO TAREFA](https://projeto-tarefa.herokuapp.com/)

## Laboratório Banco de Dados II

![Heroku](heroku-logo-solid-gradient.png)


### Autor

- Jefferson Lisboa (ra00128087@pucsp.edu.br)


## Descrição

- Cada grupo deverá uma apresentação sobre as soluções de criação e gerenciamento de bancos de dados em nuvem (ex: Amazon, Heroku etc). É importante conter um tutorial sobre como realizar tarefas práticas. Cada membro do grupo deve apresentar uma tarefa diferente.

## Informações

-   Projeto Desenvolvido utilizando tecnologia Java com Spring Web e banco de dados postgres oferecido pela PaaS(Plataforma como serviço) Heroku.

## Configurações

| Descrição     | Informações   |
| -----------   | -----------   |
| Java          | openjdk-8     |
| Spring Web    | 2.1 RELEASE   | 
| Maven         | 3-6.0-embed   |
| Git           | 2.19.1        |
| Gitlab Runner | 9.0           |
| Docker        | 18.+ CE       |
| Compose       | 1.21 / 3.4.1  |
| Container     | openjdk:alpine|

## Iniciando localhost para desenvolvimento

```sh
bash run.sh #automatizado
```

ou manual

```sh
bash mvnw clean
bash mvnw package -DskipTests
docker-compose -f docker-compose.yml up --build # iniciar http://localhost:8080
docker-compose -f docker-compose.yml down # Após realizar ctrl+c, podemos remover os serviços.
```

ou avançado

```sh

bash mvnw clean
bash mvnw package -DskipTests

docker-compose -f docker-compose-database.yml up -d # subir container de banco de dados e trabalhar isoladamente no projeto.

JDBC_DATABASE_URL="jdbc:postgresql://localhost:9080/tarefa" \
JDBC_DATABASE_USERNAME="tarefa" \
JDBC_DATABASE_PASSWORD="password" \
PORT=8080 \
java -jar target/*.war

docker-compose -f docker-compose-database.yml down # após ctrl+c no projeto java, delete serviço de banco de dados de docker-compose.

```
---

## Tutorial

### Primeiro Ato
- Criar conta de acesso em [heroku](http:www.heroku.com)
    
### Segundo Ato
-  Criar uma [aplicação](https://dashboard.heroku.com/new-app)

### Terceiro Ato
-  Adicionar complemento de banco de dados [postgres](http://elements.heroku.com/addons/heroku-postgresql)
        
-  Informar aplicação que terá conectividade de acesso ao banco de dados

### Quarto Ato
- Acessar portal do framework [Spring.io](https://start.spring.io/) e gerar projeto base
- Deve configurá-lo com as seguintes dependências:
    -   Web
    -   JPA
    -   JDBC
    -   PostgreSQL 

### Sexto Ato
-   Configurar [Spring](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-sql.html) para utilizar o SGBD PostgreSQL
-   Arquivo principal de configuração é o [application.properties](src/main/resources/application.properties)

### Sétimo Ato
-   Configurar [pipeline do gitlab](https://docs.gitlab.com/ee/ci/pipelines.html) para automatizar deploy para heroku.
-   Configurar [TOKEN_API_HEROKU](https://devcenter.heroku.com/articles/authentication) nas variavéis de ambiente do gitlab para realizar deploy

### Oitavo Ato
-   Realizar [Commit](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html) e esperar esteira de deploy

---

## Discussão

Heroku oferece uma forma de [conectividade](https://devcenter.heroku.com/articles/connecting-to-relational-databases-on-heroku-with-java) com seu complemento de banco de dados postgreSQL bem interessante para o framework Spring.

Toda aplicação conectada com o banco de dados recebe uma variável de ambiente **DATABASE_URL**. Esta variável as seguintes informações:

    -   localidade do banco de dados na nuvem
    -   porta de acesso
    -   usuário de acesso
    -   senha de acesso
    -   nome do banco de dados

Para o desenvolvimento do projeto-tarefa, foi necessário configurar [DatabaseConfig.java](src/main/java/br/com/projeto/tarefa/DatabaseConfig.java)

```java

        @Bean
	public DataSource dataSource() {

		BasicDataSource basicDataSource;

		try {
			basicDataSource = databaseURL();
		} catch (Exception e) {

			basicDataSource = new BasicDataSource();

			basicDataSource.setUrl(System.getenv("JDBC_DATABASE_URL"));
			basicDataSource.setUsername(System.getenv("JDBC_DATABASE_USERNAME"));
			basicDataSource.setPassword(System.getenv("JDBC_DATABASE_PASSWORD"));

		}

		return basicDataSource;

	}

```

Esse método tem como principal objetivo, transformar a URL de acesso ao postgreSQL para o formato padrão de acesso a banco de dados pela biblioteca spring JDBC.

A partir dessa configuração, não é mais nessário em nível de cliente ter a preocupação com a forma de conexão com o banco de dados, seja em nível de desenvolvimento local ou na nuvem.

---

Um exemplo de application.properties para um projeto comum possui basicamente essa estrutura

```properties

# URL do banco de dados. Pode ser em nuvem ou local
spring.datasource.url=jdbc:postgresql://URL:5432/BANCO_DE_DADOS_NOME

# Configuração do nome de usuário
spring.datasource.username= usuario

# Configuração da senha de usuario
spring.datasource.password= senha

# Dialeto que o hibernate utilizará para comunicação com o SGDB
spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.PostgreSQLDialect

# Estilo de interação com banco de dados. Posso escolher entre opções de sempre apagar os dados quando a aplicação iniciar, atualizar dados caso exista ou não fazer nada.
spring.jpa.hibernate.ddl-auto = update

```